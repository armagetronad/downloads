#!/bin/bash

set -x

image_base=registry.gitlab.com/armagetronad/downloads/jekyll
image=${image_base}:0290_0

# build
docker build . -t ${image}

# upload, extract digest
logfile=`tempfile`
docker push ${image} | tee ${logfile}
grep "digest:" ${logfile} || exit 0
digest=`grep "digest:" ${logfile} | sed -e "s/.*digest: //" -e "s/ .*//"`

# paste digest into CI file
sed -i .gitlab-ci.yml -e "s,^image: ${image_base}.*,image: ${image_base}@${digest},"
