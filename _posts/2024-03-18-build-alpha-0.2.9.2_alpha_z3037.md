---
layout: post
title:  "Build 0.2.9.2_alpha_z3037 available"
date:   2024-03-18 14:46:40 +0000
categories: build alpha

version: 0.2.9.2_alpha_z3037

series: CURRENT
suffix: alpha
git_rev: 62481390ca653d1f21d8ec880b3345bda82da88b
git_reference: legacy_0.2.9

zeroinstall_branch: alpha
steam_branch: alpha
ppa_branch: alpha

uri_base: https://download.armagetronad.org/staging/alpha/2024/0.2.9.2_alpha_z3037/

uri_winclient: armagetronad-alpha-0.2.9.2_alpha_z3037.win32.exe
uri_lin64client: ArmagetronAlpha.AppImage
uri_lin32client: ArmagetronAlpha-32bit.AppImage
uri_macosclient: armagetronad-alpha-0.2.9.2_alpha_z3037.dmg

uri_winserver: armagetronad-alpha-dedicated-0.2.9.2_alpha_z3037.win32.exe
uri_lin64server: ArmagetronAlphaDedicated.AppImage
uri_lin32server: ArmagetronAlphaDedicated-32bit.AppImage
uri_macosserver: armagetronad-alpha-dedicated-0.2.9.2_alpha_z3037.dmg

uri_tarsrc: armagetronad-alpha-0.2.9.2_alpha_z3037.tbz
uri_zipsrc: armagetronad-alpha-source-0.2.9.2_alpha_z3037.zip

---

{% include alpha_build_v1.md %}

### Release Notes

This is from the 0.2.9 branch of development. Our current releases are derived from here.

## Return of the Mac

[macOS](https://wiki.armagetronad.org/index.php?title=MacOS) builds are now 
considered stable. Any still open macOS specific issues can be viewed 
[on our tracker](https://gitlab.com/armagetronad/armagetronad/-/issues?label_name%5B%5D=macOS).

There is not much else to this release. Mostly bugfixes, adaptions to
new environments, fixes to the build system that bring it more in line with
standards, code quality improvements from fixing all the warnings the
macOS compiler raised.

There is now a VSYNC enable/disable/default option in the display system setup
backported from trunk! That probably was a Z-Man specific itch. Changes in Linux
land switched the default from On to Off, and it is annoying to override with
the command line.

Changes in AppImage Land: Our AppImage files now are signed and support bandwidth-saving updates via [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). They no longer carry their version in the filename because after an update, that would be a lie.

We now support installation via Flatpak for the 64-bit Linux client. Get the stable versions from [Flathub](https://flathub.org/apps/details/org.armagetronad.ArmagetronAdvanced); [our own repository](https://download.armagetronad.org/docs/flatpak/) has those and also carries the usual test builds.


### Patch Notes


#### Changes since 0.2.9.2.3:

##### Fixed Bugs

 * FPS display inaccurate ([#143](https://gitlab.com/armagetronad/armagetronad/-/issues/143))

##### Contributors

Manuel Moos
