---
layout: post
title:  "0.2.9.0 released"
date:   2020-07-29 17:03:46 +0000
categories: release build stable blog

version: 0.2.9.0

series: CURRENT
suffix: armagetronad
git_rev: ffcb07c3339e5c164d9839f010491281ba35bb2e
git_reference: v0.2.9.0

zeroinstall_branch: stable
steam_branch: staging
ppa_branch: armagetronad

uri_base: https://launchpad.net/armagetronad/0.2.9/0.2.9.0/+download/

uri_winclient: armagetronad-0.2.9.0.gcc.win32.exe
uri_lin64client: ArmagetronAdvanced-0.2.9.0
uri_lin32client: ArmagetronAdvanced-32bit-0.2.9.0

uri_winserver: armagetronad-dedicated-0.2.9.0.gcc.win32.exe
uri_lin64server: ArmagetronAdvancedDedicated-0.2.9.0
uri_lin32server: ArmagetronAdvancedDedicated-32bit-0.2.9.0

uri_tarsrc: armagetronad-0.2.9.0.tbz
uri_zipsrc: armagetronad-source-0.2.9.0.zip

---

We never intended to release a 0.2.9 version, instead the plan was to go directly to 0.4. But 0.4 got stalled, badly. And we had continued improving the
source on the 0.2.8 branch little by little. Then Covid-19 came around, forcing a lot of people to stay at home, which gave us the motivation
to release the game on [Steam](https://store.steampowered.com/app/1306180) and [itch.io](https://armagetronad.itch.io/armagetronad). That's basically why this version exists, so we did not have to publish a 0.2.8.3 based version there.

Enjoy!

Major changes since 0.2.8.3 on the game client are better readability for the console output, text input and ingame menu, better initial configuration, keyboard layout reminders
and an initial, slightly easier match against an AI.

Major addition on the server are external scripts; they get fed the ladderlog output and their output is parsed as console input. The ladderlog output has been expanded
to make that feature more useful. Since these scripts are launched as external processes, they can be written in any language.

We will continue to publish security updates and adaptions to new compilers and Linux versions for the 0.2.8.3 series. Linux distributions with a focus on feature stability
should stick to that for the time being. We transition softly into a more continuous delivery model with this release, further 0.2.9.X will bring more changes.

{% include stable_build_v1.md %}

### Patch Notes


#### Changes since 0.2.8.3.5:

##### Fixed Bugs

 * Windows: DPI scaling behavior wrong ([#33](https://gitlab.com/armagetronad/armagetronad/-/issues/33))
 * Debian builds: Documentation html files have empty last change ([#9](https://gitlab.com/armagetronad/armagetronad/-/issues/9))
 * Socket and unclean exit trouble due to fork(), execve() and exit() interaction ([#13](https://gitlab.com/armagetronad/armagetronad/-/issues/13))
 * Ubuntu PPA release candidate builds would overwrite previous stable releases ([#15](https://gitlab.com/armagetronad/armagetronad/-/issues/15))
 * Language string identifier silence_player_text used twice ([#22](https://gitlab.com/armagetronad/armagetronad/-/issues/22))
 * Bad video mode error recovery ([#23](https://gitlab.com/armagetronad/armagetronad/-/issues/23))
 * AppRun script does not work if call path contains spaces ([#24](https://gitlab.com/armagetronad/armagetronad/-/issues/24))
 * Included forum links outdated ([#25](https://gitlab.com/armagetronad/armagetronad/-/issues/25))
 * Client segfaults on OpenBSD ([#26](https://gitlab.com/armagetronad/armagetronad/-/issues/26))
 * Client compiled with clang 10.0 (optimized) sefgaults on logout ([#28](https://gitlab.com/armagetronad/armagetronad/-/issues/28))
 * Valgrind reports uses of unitialized, freed or invalid memory ([#29](https://gitlab.com/armagetronad/armagetronad/-/issues/29))

##### New Features

 * Update German translation ([#2](https://gitlab.com/armagetronad/armagetronad/-/issues/2))
 * Adapt ChangeLog and fingerprint generation to git ([#11](https://gitlab.com/armagetronad/armagetronad/-/issues/11))
 * Integrated build system ([#14](https://gitlab.com/armagetronad/armagetronad/-/issues/14))
 * Make playback time display optional ([#16](https://gitlab.com/armagetronad/armagetronad/-/issues/16))
 * Deploy to itch.io ([#19](https://gitlab.com/armagetronad/armagetronad/-/issues/19))
 * Credit contributors in patch notes ([#21](https://gitlab.com/armagetronad/armagetronad/-/issues/21))
 * Added detection and reaction code for timing assist bots
 * /shuffle now works before you actually join a team
 * Implemented /shout command and associated settings
 * Fixed suspension and silenced status not being re-applied after a player
   disconnects and rejoins a server.
 * Tweaks to enemy influence system.
 * Ingame menu and console now are drawn on top of a semi-transparent,
   darkened area for increased readability.
 * New first start menu with clearer language selection and initial setup.
 * Tutorial match against one AI at slower speed.
 * Tutorial tooltips for the most important keybindings.
 * ADD_MASTER_SERVER command to announce a server to a new master server.
   Simplifies the process to list your server on a subculture.
 * Team launch positions logged to ladderlog with POSITIONS event. Disabled by
   default.
 * New command-line option "--input" added to poll for input from a file
   instead of stdin.
 * Added WHITELIST_ENEMIES_[IP/USERNAME] to allow players to be enemies, even
   if they come from the same IP address and ALLOW_ENEMIES_SAME_IP is
   disabled (which is its default setting).
 * GAME_END, NEW_MATCH, and NEW_ROUND ladderlog events include date and time.
 * Added ENCODING ladderlog event, which specifies the encoding for data in
   ladderlog.txt.
 * "--input" now can be used more than once to read from multiple files or pipes.
 * new team management ladderlog messages:

        TEAM_CREATED <team name>
        TEAM_DESTROYED <team name>
        TEAM_RENAMED <old team name> <new team name>
        TEAM_PLAYER_ADDED <team name> <player>
        TEAM_PLAYER_REMOVED <team name> <player>

 * Manage external scripts on Unix dedicated servers. New commands: 
   SPAWN_SCRIPT, RESPAWN_SCRIPT, FORCE_RESPAWN_SCRIPT, KILL_SCRIPT,
   LIST_SCRIPTS, SCRIPT_ENV.
 * New setting ACCESS_LEVEL_ANNOUNCE_LOGIN that determines if a player's
   login/logout message can be announced.
 * Authentication is now enabled by default.

##### Other Changes

 * Make custom camera the default for new players ([#35](https://gitlab.com/armagetronad/armagetronad/-/issues/35))

##### Contributors

Daniel Harple, Hugh McMaster, Luke-Jr, Manuel Moos, Uzix, fman23, zolk3ri
