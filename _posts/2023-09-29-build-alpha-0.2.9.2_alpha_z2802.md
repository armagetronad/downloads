---
layout: post
title:  "Build 0.2.9.2_alpha_z2802 available"
date:   2023-09-29 22:16:54 +0000
categories: build alpha

version: 0.2.9.2_alpha_z2802

series: CURRENT
suffix: alpha
git_rev: 0560c0b2ce3c5ebef0d77d55cf7f0d4d6fb2ce44
git_reference: legacy_0.2.9

zeroinstall_branch: alpha
steam_branch: alpha
ppa_branch: alpha

uri_base: https://download.armagetronad.org/staging/alpha/2023/0.2.9.2_alpha_z2802/

uri_winclient: armagetronad-alpha-0.2.9.2_alpha_z2802.win32.exe
uri_lin64client: ArmagetronAlpha.AppImage
uri_lin32client: ArmagetronAlpha-32bit.AppImage
uri_macosclient: armagetronad-alpha-0.2.9.2_alpha_z2802.dmg

uri_winserver: armagetronad-alpha-dedicated-0.2.9.2_alpha_z2802.win32.exe
uri_lin64server: ArmagetronAlphaDedicated.AppImage
uri_lin32server: ArmagetronAlphaDedicated-32bit.AppImage
uri_macosserver: armagetronad-alpha-dedicated-0.2.9.2_alpha_z2802.dmg

uri_tarsrc: armagetronad-alpha-0.2.9.2_alpha_z2802.tbz
uri_zipsrc: armagetronad-alpha-source-0.2.9.2_alpha_z2802.zip

---

{% include alpha_build_v1.md %}

### Release Notes

This is from the 0.2.9 branch of development. Our current releases are derived from here.

Changes in AppImage Land: Our AppImage files now are signed and support bandwidth-saving updates via [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). They no longer carry their version in the filename because after an update, that would be a lie.

We now support installation via Flatpak for the 64-bit Linux client. Get the stable versions from [Flathub](https://flathub.org/apps/details/org.armagetronad.ArmagetronAdvanced); [our own repository](https://download.armagetronad.org/docs/flatpak/) has those and also carries the usual test builds.

We started to integrate builds for [macOS](https://wiki.armagetronad.org/index.php?title=MacOS).
Consider them alpha for now, testing and patches very welcome. The current macOS specific issues
can be viewed [on our tracker](https://gitlab.com/armagetronad/armagetronad/-/issues?label_name%5B%5D=macOS).

0.2.9.1.1 worked around a crash bug in the (otherwise splendid) sdl12-compat library.

### Patch Notes


#### Changes since 0.2.9.1.1:

##### Fixed Bugs

 * Work around Ubuntu sending spurious window activation messages ([#68](https://gitlab.com/armagetronad/armagetronad/-/issues/68))
 * Desktop/AppData/Icon files are installed in a wrong place during make install ([#73](https://gitlab.com/armagetronad/armagetronad/-/issues/73))
 * Steam: No logging, does not work in pressure vessel ([#77](https://gitlab.com/armagetronad/armagetronad/-/issues/77))
 * Retina display not working correctly ([#88](https://gitlab.com/armagetronad/armagetronad/-/issues/88))
 * Window title sometimes SDL_APP ([#102](https://gitlab.com/armagetronad/armagetronad/-/issues/102))
 * Docker Image: Increase thread stack size ([#104](https://gitlab.com/armagetronad/armagetronad/-/issues/104))
 * Client sends quickly typed chat message with last character missing ([#106](https://gitlab.com/armagetronad/armagetronad/-/issues/106))
 * Fix invulnerability bug handling ([#111](https://gitlab.com/armagetronad/armagetronad/-/issues/111))
 * User data saved in the wrong place on macOS ([#125](https://gitlab.com/armagetronad/armagetronad/-/issues/125))

##### New Features

 * Add AppImage zsync update data ([#78](https://gitlab.com/armagetronad/armagetronad/-/issues/78))
 * Provide docker image for the dedicated server ([#79](https://gitlab.com/armagetronad/armagetronad/-/issues/79))
 * Sign the application bundle ([#89](https://gitlab.com/armagetronad/armagetronad/-/issues/89))

##### Other Changes

 * Sound trouble deja entendu ([#130](https://gitlab.com/armagetronad/armagetronad/-/issues/130))
 * Flatpak adaptions ([#75](https://gitlab.com/armagetronad/armagetronad/-/issues/75))
 * Add macOS dmg bundling ([#86](https://gitlab.com/armagetronad/armagetronad/-/issues/86))
 * Integrate macOS build into pipeline ([#87](https://gitlab.com/armagetronad/armagetronad/-/issues/87))
 * 0.2.9 clients lose connection on 0.4 server ([#98](https://gitlab.com/armagetronad/armagetronad/-/issues/98))
 * Backslashes not escaped when saving to user.cfg ([#110](https://gitlab.com/armagetronad/armagetronad/-/issues/110))
 * macOS DMG Build ([#119](https://gitlab.com/armagetronad/armagetronad/-/issues/119))
 * error: use of undeclared identifier 'finite'; did you mean 'isfinite'? ([#120](https://gitlab.com/armagetronad/armagetronad/-/issues/120))

##### Contributors

Armanelgtron, Cosimo Cecchi, Manuel Moos
