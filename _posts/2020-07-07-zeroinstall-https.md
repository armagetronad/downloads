---
layout: post
title:  "Zero Install feeds now served over https"
date:   2020-07-07 18:56:06 +0200
categories: blog
---

### Adapt your subscriptions!

Previously, our Zero Install feeds have been served unencrypted via http. 
That was fine and secure enough, since the content itself is already cryptographically signed and secured.
However, https is rapidly becoming the norm for everything and potential future browsers may even completely
refuse to accept plain http. So we switched.

Unfortunately, the switch is not seamless for you; Zero Install feeds have their canonical URI encoded in the
document and the URI you fetch them from must match the canonical URI. It's either http or https in this case,
we can't (easily) have both. So you will have to adapt the URIs if you are already subscribed.

[See the installation instructions]({% link _docs/install.md %}#zero-install) for updated URIs. 
Or just replace http with https, that works, too.

This is also a good time to remind everyone that Zero Install is currently the best way to always play up to date
versions of Armagetron Advanced, it works both on Linux and Windows. The launcher is very lightweight and fast, unlike upcoming
Steam and even itch.io.