---
layout: post
title:  "0.2.9.0.1 released"
date:   2020-08-14 13:05:28 +0000
categories: release build stable

version: 0.2.9.0.1

series: CURRENT
suffix: armagetronad
git_rev: a43562e4af733d8b4f75f6eb2ddbb81b07867790
git_reference: v0.2.9.0.1

zeroinstall_branch: stable
steam_branch: staging
ppa_branch: armagetronad

uri_base: https://launchpad.net/armagetronad/0.2.9/0.2.9.0.1/+download/

uri_winclient: armagetronad-0.2.9.0.1.gcc.win32.exe
uri_lin64client: ArmagetronAdvanced-0.2.9.0.1
uri_lin32client: ArmagetronAdvanced-32bit-0.2.9.0.1

uri_winserver: armagetronad-dedicated-0.2.9.0.1.gcc.win32.exe
uri_lin64server: ArmagetronAdvancedDedicated-0.2.9.0.1
uri_lin32server: ArmagetronAdvancedDedicated-32bit-0.2.9.0.1

uri_tarsrc: armagetronad-0.2.9.0.1.tbz
uri_zipsrc: armagetronad-source-0.2.9.0.1.zip

---

This release completes the library content of the AppImage files and Zero Install archives to make them compatible
with more Linux distributions. You only need to upgrade if you are on Linux and would like to use the AppImage
distribution. Affected Zero Install users get upgraded automatically, everyone else was not affected and can stick
to [0.2.9.0]({% link _posts/2020-07-29-build-stable-0.2.9.0.md %}).

{% include stable_build_v1.md %}

### Patch Notes


#### Changes since 0.2.9.0:

##### Fixed Bugs

 * Can't open the .appimage file. Missing libwebp.so.5 ([#54](https://gitlab.com/armagetronad/armagetronad/-/issues/54))

##### Contributors

Manuel Moos
