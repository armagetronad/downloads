---
layout: post
title:  "Build 0.2.9.2_rc_z3006 available"
date:   2024-03-17 22:56:19 +0000
categories: build rc

version: 0.2.9.2_rc_z3006

series: CURRENT
suffix: armagetronad
git_rev: fab4f62d9f3687415d376fc0827b622c58676266
git_reference: release_0.2.9

zeroinstall_branch: rc
steam_branch: staging
ppa_branch: armagetronad

uri_base: https://download.armagetronad.org/staging/rc/2024/0.2.9.2_rc_z3006/

uri_winclient: armagetronad-0.2.9.2_rc_z3006.win32.exe
uri_lin64client: ArmagetronAdvanced.AppImage
uri_lin32client: ArmagetronAdvanced-32bit.AppImage
uri_macosclient: armagetronad-0.2.9.2_rc_z3006.dmg

uri_winserver: armagetronad-dedicated-0.2.9.2_rc_z3006.win32.exe
uri_lin64server: ArmagetronAdvancedDedicated.AppImage
uri_lin32server: ArmagetronAdvancedDedicated-32bit.AppImage
uri_macosserver: armagetronad-dedicated-0.2.9.2_rc_z3006.dmg

uri_tarsrc: armagetronad-0.2.9.2_rc_z3006.tbz
uri_zipsrc: armagetronad-source-0.2.9.2_rc_z3006.zip

---

{% include rc_build_v1.md %}

### Release Notes

This is from the 0.2.9 branch of development. Our current releases are derived from here.

## Return of the Mac

[macOS](https://wiki.armagetronad.org/index.php?title=MacOS) builds are now 
considered stable. Any still open macOS specific issues can be viewed 
[on our tracker](https://gitlab.com/armagetronad/armagetronad/-/issues?label_name%5B%5D=macOS).

There is not much else to this release. Mostly bugfixes, adaptions to
new environments, fixes to the build system that bring it more in line with
standards, code quality improvements from fixing all the warnings the
macOS compiler raised.

There is now a VSYNC enable/disable/default option in the display system setup
backported from trunk! That probably was a Z-Man specific itch. Changes in Linux
land switched the default from On to Off, and it is annoying to override with
the command line.

Changes in AppImage Land: Our AppImage files now are signed and support bandwidth-saving updates via [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). They no longer carry their version in the filename because after an update, that would be a lie.

We now support installation via Flatpak for the 64-bit Linux client. Get the stable versions from [Flathub](https://flathub.org/apps/details/org.armagetronad.ArmagetronAdvanced); [our own repository](https://download.armagetronad.org/docs/flatpak/) has those and also carries the usual test builds.


### Patch Notes


#### Changes since 0.2.9.1.1:

##### Fixed Bugs

 * Sound trouble deja entendu ([#130](https://gitlab.com/armagetronad/armagetronad/-/issues/130))
 * Docker/Alpine: Crash in ZThread::ThreadOps::setPriority ([#138](https://gitlab.com/armagetronad/armagetronad/-/issues/138))
 * Display lists keep rim wall lowered ([#141](https://gitlab.com/armagetronad/armagetronad/-/issues/141))
 * FPS display inaccurate ([#143](https://gitlab.com/armagetronad/armagetronad/-/issues/143))
 * Online play suddenly stopped working. ([#145](https://gitlab.com/armagetronad/armagetronad/-/issues/145))
 * Work around Ubuntu sending spurious window activation messages ([#68](https://gitlab.com/armagetronad/armagetronad/-/issues/68))
 * Desktop/AppData/Icon files are installed in a wrong place during make install ([#73](https://gitlab.com/armagetronad/armagetronad/-/issues/73))
 * Steam: No logging, does not work in pressure vessel ([#77](https://gitlab.com/armagetronad/armagetronad/-/issues/77))
 * Retina display not working correctly ([#88](https://gitlab.com/armagetronad/armagetronad/-/issues/88))
 * 0.2.9 clients lose connection on 0.4 server ([#98](https://gitlab.com/armagetronad/armagetronad/-/issues/98))
 * Window title sometimes SDL_APP ([#102](https://gitlab.com/armagetronad/armagetronad/-/issues/102))
 * Docker Image: Increase thread stack size ([#104](https://gitlab.com/armagetronad/armagetronad/-/issues/104))
 * Client sends quickly typed chat message with last character missing ([#106](https://gitlab.com/armagetronad/armagetronad/-/issues/106))
 * Backslashes not escaped when saving to user.cfg ([#110](https://gitlab.com/armagetronad/armagetronad/-/issues/110))
 * Fix invulnerability bug handling ([#111](https://gitlab.com/armagetronad/armagetronad/-/issues/111))
 * macOS DMG Build problems when building inside the source tree ([#119](https://gitlab.com/armagetronad/armagetronad/-/issues/119))
 * error: use of undeclared identifier 'finite'; did you mean 'isfinite'? ([#120](https://gitlab.com/armagetronad/armagetronad/-/issues/120))
 * User data saved in the wrong place on macOS ([#125](https://gitlab.com/armagetronad/armagetronad/-/issues/125))

##### New Features

 * Make network trafic realtime priority ([#137](https://gitlab.com/armagetronad/armagetronad/-/issues/137))
 * Add AppImage zsync update data ([#78](https://gitlab.com/armagetronad/armagetronad/-/issues/78))
 * Provide docker image for the dedicated server ([#79](https://gitlab.com/armagetronad/armagetronad/-/issues/79))
 * Add macOS dmg bundling ([#86](https://gitlab.com/armagetronad/armagetronad/-/issues/86))
 * Integrate macOS build into pipeline ([#87](https://gitlab.com/armagetronad/armagetronad/-/issues/87))
 * Sign the application bundle ([#89](https://gitlab.com/armagetronad/armagetronad/-/issues/89))

##### Other Changes

 * Flatpak adaptions ([#75](https://gitlab.com/armagetronad/armagetronad/-/issues/75))

##### Contributors

Armanelgtron, Cosimo Cecchi, Manuel Moos
