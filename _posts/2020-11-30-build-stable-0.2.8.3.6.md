---
layout: post
title:  "0.2.8.3.6 released"
date:   2020-11-30 20:55:01 +0000
categories: release build stable

version: 0.2.8.3.6

series: LTS_0283
suffix: armagetronad
git_rev: 8ebeb0b5f1846f5967b4e94ac1ded1f677d28743
git_reference: v0.2.8.3.6

zeroinstall_branch: stable
steam_branch: 
ppa_branch: armagetronad

uri_base: https://launchpad.net/armagetronad/0.2.8/0.2.8.3.6/+download/

uri_winclient: armagetronad-0.2.8.3.6.win32.exe
uri_lin64client: ArmagetronAdvanced-0.2.8.3.6.AppImage
uri_lin32client: ArmagetronAdvanced-32bit-0.2.8.3.6.AppImage

uri_winserver: armagetronad-dedicated-0.2.8.3.6.win32.exe
uri_lin64server: ArmagetronAdvancedDedicated-0.2.8.3.6.AppImage
uri_lin32server: ArmagetronAdvancedDedicated-32bit-0.2.8.3.6.AppImage

uri_tarsrc: armagetronad-0.2.8.3.6.tbz
uri_zipsrc: armagetronad-source-0.2.8.3.6.zip

---

{% include stable_build_v1.md %}

### Release Notes

This is from the 0.2.8.3 branch of development. 
It is the most recent LTS (Long Term Support) branch.
While it is the most recent LTS branch, 
it will receive security fixes, adaptions to new
compilers and Linux versions and some less important bugfixes.

Target audience for this series are mostly Linux distributors who do not want 
to upgrade to 0.2.9 just yet.
If you are in that camp and need help integrating the changes into your build, 
contact us.

0.2.8.3.6 is the first release using the build system that powers the 0.2.9 series. 
It should bring more consistency between this and future releases, 
but as newer versions of autotools are now used, differences to 0.2.8.3.5 in the tarball
are to be expected.

Security related: One fix for a remotely exploitable use after free was backported from 0.2.9 (#34) and one
client crash exploitable by making you play back a specially prepared recording (#54) was fixed.

### Patch Notes


#### Changes since 0.2.8.3.5:

##### Fixed Bugs

 * Windows: DPI scaling behavior wrong ([#33](https://gitlab.com/armagetronad/armagetronad/-/issues/33))
 * Crash in menu key repeat handling ([#66](https://gitlab.com/armagetronad/armagetronad/-/issues/66))
 * Use after free in nNetObject::ClearKnows ([#34](https://gitlab.com/armagetronad/armagetronad/-/issues/34))
 * Client segfaults on OpenBSD ([#26](https://gitlab.com/armagetronad/armagetronad/-/issues/26))
 * Debian builds: Documentation html files have empty last change ([#9](https://gitlab.com/armagetronad/armagetronad/-/issues/9))
 * Ubuntu PPA release candidate builds would overwrite previous stable releases ([#15](https://gitlab.com/armagetronad/armagetronad/-/issues/15))
 * Can't open the .appimage file. Missing libwebp.so.5 ([#54](https://gitlab.com/armagetronad/armagetronad/-/issues/54))
 * AppRun script does not work if call path contains spaces ([#24](https://gitlab.com/armagetronad/armagetronad/-/issues/24))
 * Included forum links outdated ([#25](https://gitlab.com/armagetronad/armagetronad/-/issues/25))
 * Missing null check for master server info ([#58](https://gitlab.com/armagetronad/armagetronad/-/issues/58))
 * Client compiled with clang 10.0 (optimized) sefgaults on logout ([#28](https://gitlab.com/armagetronad/armagetronad/-/issues/28))
 * Valgrind reports uses of unitialized, freed or invalid memory ([#29](https://gitlab.com/armagetronad/armagetronad/-/issues/29))
 * AppImage library content still lacking ([#63](https://gitlab.com/armagetronad/armagetronad/-/issues/63))

##### New Features

 * Adapt ChangeLog and fingerprint generation to git ([#11](https://gitlab.com/armagetronad/armagetronad/-/issues/11))
 * Integrated build system ([#14](https://gitlab.com/armagetronad/armagetronad/-/issues/14))

##### Contributors

Hugh McMaster, Manuel Moos
