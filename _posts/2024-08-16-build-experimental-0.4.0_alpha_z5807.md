---
layout: post
title:  "Build 0.4.0_alpha_z5807 available"
date:   2024-08-16 13:58:14 +0000
categories: build experimental

version: 0.4.0_alpha_z5807

series: EXPERIMENTAL
suffix: experimental
git_rev: e9f81456f568eee3bbcd12b3430ff3446ce05956
git_reference: trunk

zeroinstall_branch: experimental
steam_branch: experimental
ppa_branch: experimental

uri_base: https://download.armagetronad.org/staging/snapshots/2024/0.4.0_alpha_z5807/

uri_winclient: armagetronad-experimental-0.4.0_alpha_z5807.win32.exe
uri_lin64client: ArmagetronExperimental.AppImage
uri_lin32client: ArmagetronExperimental-32bit.AppImage
uri_macosclient: armagetronad-experimental-0.4.0_alpha_z5807.dmg

uri_winserver: armagetronad-experimental-dedicated-0.4.0_alpha_z5807.win32.exe
uri_lin64server: ArmagetronExperimentalDedicated.AppImage
uri_lin32server: ArmagetronExperimentalDedicated-32bit.AppImage
uri_macosserver: armagetronad-experimental-dedicated-0.4.0_alpha_z5807.dmg

uri_tarsrc: armagetronad-experimental-0.4.0_alpha_z5807.tbz
uri_zipsrc: armagetronad-experimental-source-0.4.0_alpha_z5807.zip

---

{% include experimental_build_v1.md %}

### Release Notes

This is from the 0.4 branch of development. Our experimental releases are derived from here.

## Return of the Mac

[macOS](https://wiki.armagetronad.org/index.php?title=MacOS) builds are now 
considered stable. Any still open macOS specific issues can be viewed 
[on our tracker](https://gitlab.com/armagetronad/armagetronad/-/issues?label_name%5B%5D=macOS).

There is not much else to this release. Mostly bugfixes, adaptions to
new environments, fixes to the build system that bring it more in line with
standards, code quality improvements from fixing all the warnings the
macOS compiler raised.

There is now a VSYNC enable/disable/default option in the display system setup
backported from trunk! That probably was a Z-Man specific itch. Changes in Linux
land switched the default from On to Off, and it is annoying to override with
the command line.

Changes in AppImage Land: Our AppImage files now are signed and support bandwidth-saving updates via [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). They no longer carry their version in the filename because after an update, that would be a lie.

We now support installation via Flatpak for the 64-bit Linux client. Get the stable versions from [Flathub](https://flathub.org/apps/details/org.armagetronad.ArmagetronAdvanced); [our own repository](https://download.armagetronad.org/docs/flatpak/) has those and also carries the usual test builds.


### Patch Notes


#### Changes since 0.2.9.2.3:

##### Fixed Bugs

 * Ubuntu PPA release candidate builds would overwrite previous stable releases ([#15](https://gitlab.com/armagetronad/armagetronad/-/issues/15))
 * Bad video mode error recovery ([#23](https://gitlab.com/armagetronad/armagetronad/-/issues/23))
 * Crash when entering any game on client on OpenBSD+clang ([#30](https://gitlab.com/armagetronad/armagetronad/-/issues/30))
 * mathexpr.cpp: exp10 not available on OpenBSD ([#31](https://gitlab.com/armagetronad/armagetronad/-/issues/31))
 * Global keyboard actions, like fullscreen toggle, triggered while entering text in text input fields ([#32](https://gitlab.com/armagetronad/armagetronad/-/issues/32))
 * Windows: DPI scaling behavior wrong ([#33](https://gitlab.com/armagetronad/armagetronad/-/issues/33))
 * AppRun script uses ~/.armagetronad as cache directory, should use ${XDG_CACHE_HOME:-~/.cache}/armagetronad ([#43](https://gitlab.com/armagetronad/armagetronad/-/issues/43))
 * Music does not work on SDL_Mixer > 2.0.1 ([#64](https://gitlab.com/armagetronad/armagetronad/-/issues/64))
 * "xdg-icon-resource: command not found" on macOS ([#83](https://gitlab.com/armagetronad/armagetronad/-/issues/83))
 * Retina display not working correctly ([#88](https://gitlab.com/armagetronad/armagetronad/-/issues/88))
 * 0.4 macOS build crashes on startup ([#94](https://gitlab.com/armagetronad/armagetronad/-/issues/94))
 * Fullscreen mode: flickering startup ([#95](https://gitlab.com/armagetronad/armagetronad/-/issues/95))
 * 0.2.9 clients lose connection on 0.4 server ([#98](https://gitlab.com/armagetronad/armagetronad/-/issues/98))
 * Docker Image: Increase thread stack size ([#104](https://gitlab.com/armagetronad/armagetronad/-/issues/104))
 * "Redo first setup": Color says "Blue", result is black. Should show "Leave alone" and do just that. ([#113](https://gitlab.com/armagetronad/armagetronad/-/issues/113))
 * Fix login prompt not accepting input immediately ([#123](https://gitlab.com/armagetronad/armagetronad/-/issues/123))
 * Display lists keep rim wall lowered ([#141](https://gitlab.com/armagetronad/armagetronad/-/issues/141))
 * FPS display inaccurate ([#143](https://gitlab.com/armagetronad/armagetronad/-/issues/143))

##### New Features

 * user.cfg fine grained upgrade system ([#61](https://gitlab.com/armagetronad/armagetronad/-/issues/61))
 * Add option for low DPI rendering ([#96](https://gitlab.com/armagetronad/armagetronad/-/issues/96))
 * Make window resizable ([#97](https://gitlab.com/armagetronad/armagetronad/-/issues/97))

##### Other Changes

 * Improve sound ([#45](https://gitlab.com/armagetronad/armagetronad/-/issues/45))
 * Pasting does not properly handle unicode input ([#51](https://gitlab.com/armagetronad/armagetronad/-/issues/51))
 * Improve deployment cancellation ([#80](https://gitlab.com/armagetronad/armagetronad/-/issues/80))
 * Crash when votes are issued ([#84](https://gitlab.com/armagetronad/armagetronad/-/issues/84))
 * Better detection for boost-thread ([#85](https://gitlab.com/armagetronad/armagetronad/-/issues/85))
 * Windows debug build crashes on startup ([#91](https://gitlab.com/armagetronad/armagetronad/-/issues/91))
 * Internal error in boost::thread::thread(const T &) [T = tMemberFunctionRunnerTemplate<nDNSResolver>] ([#115](https://gitlab.com/armagetronad/armagetronad/-/issues/115))
 * Window size sometimes not set ([#116](https://gitlab.com/armagetronad/armagetronad/-/issues/116))
 * Window drifts when toggling to fullscreen and back ([#117](https://gitlab.com/armagetronad/armagetronad/-/issues/117))
 * fadeout name improvements ([#121](https://gitlab.com/armagetronad/armagetronad/-/issues/121))
 * Crash on dedicated server startup ([#122](https://gitlab.com/armagetronad/armagetronad/-/issues/122))
 * Disable low latency mode if not waiting for vsync ([#124](https://gitlab.com/armagetronad/armagetronad/-/issues/124))
 * Trunk Windows builds fail with "random_device::random_device(const std::string%)" messagebox ([#140](https://gitlab.com/armagetronad/armagetronad/-/issues/140))
 * sdl12-compat for macOS (was: Retina resolution support) ([#147](https://gitlab.com/armagetronad/armagetronad/-/issues/147))
 * BSD: Crash on master server list load ([#151](https://gitlab.com/armagetronad/armagetronad/-/issues/151))

##### Contributors

Armanelgtron, AsciiWolf, Bernhard M. Wiedemann, Dan Church, Daniel Harple, Dave Fancella, Foster McLane, Hugh McMaster, Jip, Luke-Jr, Manuel Moos, Matias Pino, Niklas Karbaum (ai.tron), Viorel-Cătălin Răpițeanu, Vitty, Voodoo, Yann Kaiser, armagetron at ensemble-fnm.de, epsy, guru3, madmax, philippeqc, pnoexz at gmail.com, wrtlprnft, yarrt
