---
layout: post
title:  "0.2.9.1.1 released"
date:   2023-08-10 17:51:01 +0000
categories: release build stable

version: 0.2.9.1.1

series: CURRENT
suffix: armagetronad
git_rev: 568a186d1c89e08f9a144631bbcc6bf901504203
git_reference: v0.2.9.1.1

zeroinstall_branch: stable
steam_branch: staging
ppa_branch: armagetronad

uri_base: https://launchpad.net/armagetronad/0.2.9/0.2.9.1.1/+download/

uri_winclient: armagetronad-0.2.9.1.1.win32.exe
uri_lin64client: ArmagetronAdvanced-0.2.9.1.1.AppImage
uri_lin32client: ArmagetronAdvanced-32bit-0.2.9.1.1.AppImage

uri_winserver: armagetronad-dedicated-0.2.9.1.1.win32.exe
uri_lin64server: ArmagetronAdvancedDedicated-0.2.9.1.1.AppImage
uri_lin32server: ArmagetronAdvancedDedicated-32bit-0.2.9.1.1.AppImage

uri_tarsrc: armagetronad-0.2.9.1.1.tbz
uri_zipsrc: armagetronad-source-0.2.9.1.1.zip

---

{% include stable_build_v1.md %}

### Release Notes

This release welcomes our new ultrawide monitor using overlords!

And, to be frank, also the old, regular widescreen overlords. 
All this time, the game had been optimized for 4:3 or 5:4 screens, 
with menu text and HUD elements getting stretched to the side for widescreen users.
No more of that! The changes also benefit splitscreen users; 
for a horizontal split, the HUD will now no longer cover half the (split) screen.

And while we were at fixing fonts, 
the default console rendering now tries to display the bitmap font precisely as it is designed, 
pixel by pixel; 
that should make the console more readable and sharper looking for everyone.

Playing back debug recordings has been made simpler and more robust; 
the --playback command line switch is no longer required and the network code should no longer
give up when the recorded server response does not match what the playback code expects. 
Especially, this version should have no problems playing back tournament recordings made with
version 0.2.8.

What Steam users had for a while is now available for everyone: 
The onboarding process has been tweaked a little. The intial game is less frustrating, 
and the tutorial tooltips are spammed less.

Furthermore, compatibility of the generic Linux binaries has been improved, 
with more systems supported out of the box.

0.2.9.1.1 works around a crash bug in the (otherwise splendid) sdl12-compat library.
### Patch Notes


#### Changes since 0.2.9.1.0:

##### Fixed Bugs

 * Crash when using sdl12-compat ([#131](https://gitlab.com/armagetronad/armagetronad/-/issues/131))
 * Ubuntu versions contain ~ppa1~ppa1 ([#70](https://gitlab.com/armagetronad/armagetronad/-/issues/70))
 * Website update waits for launchpad uploads that do not happen ([#71](https://gitlab.com/armagetronad/armagetronad/-/issues/71))
 * Versioning script incorrectly takes current tag into account ([#72](https://gitlab.com/armagetronad/armagetronad/-/issues/72))

##### Other Changes

 * Improve deployment cancellation ([#80](https://gitlab.com/armagetronad/armagetronad/-/issues/80))
 * macOS Build ([#82](https://gitlab.com/armagetronad/armagetronad/-/issues/82))

##### Contributors

Christian Mäder, Manuel Moos
