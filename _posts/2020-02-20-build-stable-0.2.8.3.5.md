---
layout: post
title:  "0.2.8.3.5 released"
date:   2020-02-20 08:41:24 +0000
categories: release build stable lts

version: 0.2.8.3.5

series: LTS_0283
suffix: armagetronad
git_rev: 8e2705f633513452c5a6b358ab865b05a3c23331
git_reference: release_0.2.8.3

zeroinstall_branch: stable
steam_branch: 
ppa_branch: armagetronad

uri_base: https://launchpad.net/armagetronad/0.2.8/0.2.8.3.5/+download/

uri_winclient: armagetronad-0.2.8.3.5.win32.exe
uri_lin64client: ArmagetronAdvanced_0.2.8.3.5
uri_lin32client: ArmagetronAdvanced_0.2.8.3.5_32bit

uri_winserver: armagetronad-dedicated-0.2.8.3.5.win32.exe
uri_lin64server: ArmagetronAdvancedDedicated_0.2.8.3.5
uri_lin32server: ArmagetronAdvancedDedicated_0.2.8.3.5_32bit

uri_tarsrc: armagetronad-0.2.8.3.5.src.tar.bz2
uri_zipsrc: armagetronad-0.2.8.3.5.src.zip

---

{% include stable_build_v1.md %}

### Patch Notes


#### Changes since 0.2.8.3.4:

##### Fixed Bugs

 * Security fix: Check that the remote is allowed to create an object
   before creating it, not decide whether it gets to keep it after
   it already has been created and potential damage has been done.
   No arbitrary code could be executed, but a client could effectively 
   shut down a server by sending crucial objects, such as the main game
   timer or the game itself.
 * Compilation fixes for current systems.


