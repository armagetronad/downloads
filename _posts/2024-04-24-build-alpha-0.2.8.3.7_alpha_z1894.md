---
layout: post
title:  "Build 0.2.8.3.7_alpha_z1894 available"
date:   2024-04-24 12:59:28 +0000
categories: build alpha

version: 0.2.8.3.7_alpha_z1894

series: LTS_0283
suffix: alpha
git_rev: fbf4c3bc7065424e6b5e771c2e2e2abf218da203
git_reference: legacy_0.2.8.3

zeroinstall_branch: alpha
steam_branch: 
ppa_branch: alpha

uri_base: https://download.armagetronad.org/staging/alpha/2024/0.2.8.3.7_alpha_z1894/

uri_winclient: armagetronad-alpha-0.2.8.3.7_alpha_z1894.win32.exe
uri_lin64client: ArmagetronAlpha-0.2.8.3.7_alpha_z1894.AppImage
uri_lin32client: ArmagetronAlpha-32bit-0.2.8.3.7_alpha_z1894.AppImage
uri_macosclient: armagetronad-alpha-0.2.8.3.7_alpha_z1894.dmg

uri_winserver: armagetronad-alpha-dedicated-0.2.8.3.7_alpha_z1894.win32.exe
uri_lin64server: ArmagetronAlphaDedicated-0.2.8.3.7_alpha_z1894.AppImage
uri_lin32server: ArmagetronAlphaDedicated-32bit-0.2.8.3.7_alpha_z1894.AppImage
uri_macosserver: armagetronad-alpha-dedicated-0.2.8.3.7_alpha_z1894.dmg

uri_tarsrc: armagetronad-alpha-0.2.8.3.7_alpha_z1894.tbz
uri_zipsrc: armagetronad-alpha-source-0.2.8.3.7_alpha_z1894.zip

---

{% include alpha_build_v1.md %}

### Release Notes

This is from the 0.2.8.3 branch of development. 
It is the most recent LTS (Long Term Support) branch.
While it is the most recent LTS branch, 
it will receive security fixes, adaptions to new
compilers and Linux versions and some less important bugfixes.

Target audience for this series are mostly Linux distributors who do not want 
to upgrade to 0.2.9 just yet.
If you are in that camp and need help integrating the changes into your build, 
contact us.

We started to integrate builds for [macOS](https://wiki.armagetronad.org/index.php?title=MacOS).
Consider them alpha for now, testing and patches very welcome. The current macOS specific issues
can be viewed [on our tracker](https://gitlab.com/armagetronad/armagetronad/-/issues?label_name%5B%5D=macOS).

### Patch Notes


#### Changes since 0.2.8.3.6:

##### Fixed Bugs

 * Sound trouble deja entendu ([#130](https://gitlab.com/armagetronad/armagetronad/-/issues/130))
 * Online play suddenly stopped working. ([#145](https://gitlab.com/armagetronad/armagetronad/-/issues/145))
 * Ubuntu versions contain ~ppa1~ppa1 ([#70](https://gitlab.com/armagetronad/armagetronad/-/issues/70))
 * Website update waits for launchpad uploads that do not happen ([#71](https://gitlab.com/armagetronad/armagetronad/-/issues/71))
 * Versioning script incorrectly takes current tag into account ([#72](https://gitlab.com/armagetronad/armagetronad/-/issues/72))
 * Retina display not working correctly ([#88](https://gitlab.com/armagetronad/armagetronad/-/issues/88))
 * 0.2.9 clients lose connection on 0.4 server ([#98](https://gitlab.com/armagetronad/armagetronad/-/issues/98))
 * Client sends quickly typed chat message with last character missing ([#106](https://gitlab.com/armagetronad/armagetronad/-/issues/106))
 * Backslashes not escaped when saving to user.cfg ([#110](https://gitlab.com/armagetronad/armagetronad/-/issues/110))
 * Fix invulnerability bug handling ([#111](https://gitlab.com/armagetronad/armagetronad/-/issues/111))
 * macOS DMG Build problems when building inside the source tree ([#119](https://gitlab.com/armagetronad/armagetronad/-/issues/119))
 * User data saved in the wrong place on macOS ([#125](https://gitlab.com/armagetronad/armagetronad/-/issues/125))

##### New Features

 * Add macOS dmg bundling ([#86](https://gitlab.com/armagetronad/armagetronad/-/issues/86))
 * Integrate macOS build into pipeline ([#87](https://gitlab.com/armagetronad/armagetronad/-/issues/87))
 * Sign the application bundle ([#89](https://gitlab.com/armagetronad/armagetronad/-/issues/89))

##### Other Changes

 * Improve deployment cancellation ([#80](https://gitlab.com/armagetronad/armagetronad/-/issues/80))
 * macOS Build ([#82](https://gitlab.com/armagetronad/armagetronad/-/issues/82))

##### Contributors

Armanelgtron, Manuel Moos
