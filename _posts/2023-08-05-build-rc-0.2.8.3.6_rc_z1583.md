---
layout: post
title:  "Build 0.2.8.3.6_rc_z1583 available"
date:   2023-08-05 18:14:42 +0000
categories: build rc

version: 0.2.8.3.6_rc_z1583

series: LTS_0283
suffix: armagetronad
git_rev: f64271b1012c9c0639641dd5b838814305529fbc
git_reference: release_0.2.8.3

zeroinstall_branch: rc
steam_branch: 
ppa_branch: armagetronad

uri_base: https://sourceforge.net/projects/armagetronad/files/rc/0.2.8.3.6_rc_z1583/

uri_winclient: armagetronad-0.2.8.3.6_rc_z1583.win32.exe
uri_lin64client: ArmagetronAdvanced-0.2.8.3.6_rc_z1583.AppImage
uri_lin32client: ArmagetronAdvanced-32bit-0.2.8.3.6_rc_z1583.AppImage

uri_winserver: armagetronad-dedicated-0.2.8.3.6_rc_z1583.win32.exe
uri_lin64server: ArmagetronAdvancedDedicated-0.2.8.3.6_rc_z1583.AppImage
uri_lin32server: ArmagetronAdvancedDedicated-32bit-0.2.8.3.6_rc_z1583.AppImage

uri_tarsrc: armagetronad-0.2.8.3.6_rc_z1583.tbz
uri_zipsrc: armagetronad-source-0.2.8.3.6_rc_z1583.zip

---

{% include rc_build_v1.md %}

### Release Notes

This is from the 0.2.8.3 branch of development. 
It is the most recent LTS (Long Term Support) branch.
While it is the most recent LTS branch, 
it will receive security fixes, adaptions to new
compilers and Linux versions and some less important bugfixes.

Target audience for this series are mostly Linux distributors who do not want 
to upgrade to 0.2.9 just yet.
If you are in that camp and need help integrating the changes into your build, 
contact us.

0.2.8.3.6 is the first release using the build system that powers the 0.2.9 series. 
It should bring more consistency between this and future releases, 
but as newer versions of autotools are now used, differences to 0.2.8.3.5 in the tarball
are to be expected.

Security related: One fix for a remotely exploitable use after free was backported from 0.2.9 (#34) and one
client crash exploitable by making you play back a specially prepared recording (#54) was fixed.

0.2.8.3.6.1 fixes silly build system mistakes and adds ppa support for Ubuntu 21.04 and 21.10.

### Patch Notes


#### Changes since 0.2.8.3.6:

##### Fixed Bugs

 * Ubuntu versions contain ~ppa1~ppa1 ([#70](https://gitlab.com/armagetronad/armagetronad/-/issues/70))
 * Website update waits for launchpad uploads that do not happen ([#71](https://gitlab.com/armagetronad/armagetronad/-/issues/71))
 * Versioning script incorrectly takes current tag into account ([#72](https://gitlab.com/armagetronad/armagetronad/-/issues/72))

##### Other Changes

 * Improve deployment cancellation ([#80](https://gitlab.com/armagetronad/armagetronad/-/issues/80))
 * macOS Build ([#82](https://gitlab.com/armagetronad/armagetronad/-/issues/82))

##### Contributors

Manuel Moos
