---
layout: post
title:  "Build 0.2.9.3_alpha_z3120 available"
date:   2025-03-07 06:03:39 +0000
categories: build alpha

version: 0.2.9.3_alpha_z3120

series: CURRENT
suffix: alpha
git_rev: ef3564c0d73a7d95ebd1d3093d2d5a27188c35fb
git_reference: legacy_0.2.9

zeroinstall_branch: alpha
steam_branch: alpha
ppa_branch: alpha

uri_base: https://download.armagetronad.org/staging/alpha/2025/0.2.9.3_alpha_z3120/

uri_winclient: armagetronad-alpha-0.2.9.3_alpha_z3120.win32.exe
uri_lin64client: ArmagetronAlpha.AppImage
uri_lin32client: ArmagetronAlpha-32bit.AppImage
uri_macosclient: armagetronad-alpha-0.2.9.3_alpha_z3120.dmg

uri_winserver: armagetronad-alpha-dedicated-0.2.9.3_alpha_z3120.win32.exe
uri_lin64server: ArmagetronAlphaDedicated.AppImage
uri_lin32server: ArmagetronAlphaDedicated-32bit.AppImage
uri_macosserver: armagetronad-alpha-dedicated-0.2.9.3_alpha_z3120.dmg

uri_tarsrc: armagetronad-alpha-0.2.9.3_alpha_z3120.tbz
uri_zipsrc: armagetronad-alpha-source-0.2.9.3_alpha_z3120.zip

---

{% include alpha_build_v1.md %}

### Release Notes

This is from the 0.2.9 branch of development. Our current releases are derived from here.

## Return of the Mac

[macOS](https://wiki.armagetronad.org/index.php?title=MacOS) builds are now 
considered stable. Any still open macOS specific issues can be viewed 
[on our tracker](https://gitlab.com/armagetronad/armagetronad/-/issues?label_name%5B%5D=macOS).

There is not much else to this release. Mostly bugfixes, adaptions to
new environments, fixes to the build system that bring it more in line with
standards, code quality improvements from fixing all the warnings the
macOS compiler raised.

There is now a VSYNC enable/disable/default option in the display system setup
backported from trunk! That probably was a Z-Man specific itch. Changes in Linux
land switched the default from On to Off, and it is annoying to override with
the command line.

Changes in AppImage Land: Our AppImage files now are signed and support bandwidth-saving updates via [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). They no longer carry their version in the filename because after an update, that would be a lie.

We now support installation via Flatpak for the 64-bit Linux client. Get the stable versions from [Flathub](https://flathub.org/apps/details/org.armagetronad.ArmagetronAdvanced); [our own repository](https://download.armagetronad.org/docs/flatpak/) has those and also carries the usual test builds.


### Patch Notes


#### Changes since 0.2.9.2.3:

##### Fixed Bugs

 * FPS display inaccurate ([#143](https://gitlab.com/armagetronad/armagetronad/-/issues/143))
 * Ping Charity: If the lowest ping player sets it to PING_CHARITY to 0, PING_CHARITY_SERVER should be 0. ([#153](https://gitlab.com/armagetronad/armagetronad/-/issues/153))
 * Private messages sent by a user should not go to the receiver as a console out. ([#155](https://gitlab.com/armagetronad/armagetronad/-/issues/155))

##### New Features

 * MAX_FPS: Setting to limit framerate ([#154](https://gitlab.com/armagetronad/armagetronad/-/issues/154))

##### Other Changes

 * sdl12-compat for macOS (was: Retina resolution support) ([#147](https://gitlab.com/armagetronad/armagetronad/-/issues/147))

##### Contributors

Armanelgtron, Bernhard M. Wiedemann, Manuel Moos
