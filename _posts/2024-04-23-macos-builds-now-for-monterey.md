---
layout: post
title:  "macOS: Monterey now required"
date:   2024-04-23 19:43:06 +0200
categories: blog
---

Previously, we supported Movave (macOS 10.14).

We skipped over Catalina (10.15) and Big Sur (11).

Now, we require Moterey (macOS 12).

Sorry for any inconvenience. The build system has to stay 
on macOS versions that are supported by Apple an Homebrew; we can sit out 
updates while there are no changes to requirements and have done so ever since 
the builds started rolling again, but now an update to the notarization 
procedure (announced well in advance, no blame on Apple there) broke the old 
Mojave system. Just for the notarization, an upgrade to Catalina would have 
been enough, but then there was no way to get working Homebrew builds of the 
sdl2 libraries we need for that old system, so further upgrades were required.

The current release 0.2.9.2.3 is still unaffected by this change, a future
0.2.9.2.X release will be affected.
