---
layout: post
title:  "0.2.9.1.0 released"
date:   2020-11-30 22:00:19 +0000
categories: release build stable

version: 0.2.9.1.0

series: CURRENT
suffix: armagetronad
git_rev: e4e928c4d5f4ff920a02685ebce66db2c7f60236
git_reference: v0.2.9.1.0

zeroinstall_branch: stable
steam_branch: staging
ppa_branch: armagetronad

uri_base: https://launchpad.net/armagetronad/0.2.9/0.2.9.1.0/+download/

uri_winclient: armagetronad-0.2.9.1.0.win32.exe
uri_lin64client: ArmagetronAdvanced-0.2.9.1.0.AppImage
uri_lin32client: ArmagetronAdvanced-32bit-0.2.9.1.0.AppImage

uri_winserver: armagetronad-dedicated-0.2.9.1.0.win32.exe
uri_lin64server: ArmagetronAdvancedDedicated-0.2.9.1.0.AppImage
uri_lin32server: ArmagetronAdvancedDedicated-32bit-0.2.9.1.0.AppImage

uri_tarsrc: armagetronad-0.2.9.1.0.tbz
uri_zipsrc: armagetronad-source-0.2.9.1.0.zip

---

{% include stable_build_v1.md %}

### Release Notes

This release welcomes our new ultrawide monitor using overlords!

And, to be frank, also the old, regular widescreen overlords. 
All this time, the game had been optimized for 4:3 or 5:4 screens, 
with menu text and HUD elements getting stretched to the side for widescreen users.
No more of that! The changes also benefit splitscreen users; 
for a horizontal split, the HUD will now no longer cover half the (split) screen.

And while we were at fixing fonts, 
the default console rendering now tries to display the bitmap font precisely as it is designed, 
pixel by pixel; 
that should make the console more readable and sharper looking for everyone.

Playing back debug recordings has been made simpler and more robust; 
the --playback command line switch is no longer required and the network code should no longer
give up when the recorded server response does not match what the playback code expects. 
Especially, this version should have no problems playing back tournament recordings made with
version 0.2.8.

What Steam users had for a while is now available for everyone: 
The onboarding process has been tweaked a little. The intial game is less frustrating, 
and the tutorial tooltips are spammed less.

Furthermore, compatibility of the generic Linux binaries has been improved, 
with more systems supported out of the box.
### Patch Notes


#### Changes since 0.2.9.0.1:

##### Fixed Bugs

 * Use after free in nNetObject::ClearKnows ([#34](https://gitlab.com/armagetronad/armagetronad/-/issues/34))
 * Missing null check for master server info ([#58](https://gitlab.com/armagetronad/armagetronad/-/issues/58))
 * Basic screen aspect ratio failures: Text ([#59](https://gitlab.com/armagetronad/armagetronad/-/issues/59))
 * Center message issues ([#62](https://gitlab.com/armagetronad/armagetronad/-/issues/62))
 * AppImage library content still lacking ([#63](https://gitlab.com/armagetronad/armagetronad/-/issues/63))
 * Crash in menu key repeat handling ([#66](https://gitlab.com/armagetronad/armagetronad/-/issues/66))

##### New Features

 * Make AppImage builds fit for registration at AppImageHub ([#17](https://gitlab.com/armagetronad/armagetronad/-/issues/17))
 * Make SIZE_ and SPEED_FACTOR floating point instead of integer ([#20](https://gitlab.com/armagetronad/armagetronad/-/issues/20))
 * Do not show a player's own IP on the client ([#27](https://gitlab.com/armagetronad/armagetronad/-/issues/27))
 * Make --playback optional so recordings can be more easily played back ([#37](https://gitlab.com/armagetronad/armagetronad/-/issues/37))
 * Steam on Windows: Add playback registry keys so .aarec files automatically playback with a doubleclick ([#38](https://gitlab.com/armagetronad/armagetronad/-/issues/38))
 * Make clientside playback more robust ([#56](https://gitlab.com/armagetronad/armagetronad/-/issues/56))
 * user.cfg fine grained upgrade system ([#61](https://gitlab.com/armagetronad/armagetronad/-/issues/61))

##### Other Changes

 * Make custom camera the default for new players ([#35](https://gitlab.com/armagetronad/armagetronad/-/issues/35))
 * Turn down tutorial tooltip spam ([#36](https://gitlab.com/armagetronad/armagetronad/-/issues/36))
 * Onboarding game improvements ([#39](https://gitlab.com/armagetronad/armagetronad/-/issues/39))
 * ROUND_CENTER_MESSAGEs are still displayed in nCLIENT state ([#49](https://gitlab.com/armagetronad/armagetronad/-/issues/49))
 * Make Full HD players see the small font as little as possible ([#60](https://gitlab.com/armagetronad/armagetronad/-/issues/60))

##### Contributors

Armanelgtron, Manuel Moos, SwagTron
