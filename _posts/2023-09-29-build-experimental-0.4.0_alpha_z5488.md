---
layout: post
title:  "Build 0.4.0_alpha_z5488 available"
date:   2023-09-29 23:24:31 +0000
categories: build experimental

version: 0.4.0_alpha_z5488

series: EXPERIMENTAL
suffix: experimental
git_rev: 42818b25d36a932c2ca4ce39fe3bc24759e452a8
git_reference: trunk

zeroinstall_branch: experimental
steam_branch: experimental
ppa_branch: experimental

uri_base: https://download.armagetronad.org/staging/snapshots/2023/0.4.0_alpha_z5488/

uri_winclient: armagetronad-experimental-0.4.0_alpha_z5488.win32.exe
uri_lin64client: ArmagetronExperimental.AppImage
uri_lin32client: ArmagetronExperimental-32bit.AppImage
uri_macosclient: armagetronad-experimental-0.4.0_alpha_z5488.dmg

uri_winserver: armagetronad-experimental-dedicated-0.4.0_alpha_z5488.win32.exe
uri_lin64server: ArmagetronExperimentalDedicated.AppImage
uri_lin32server: ArmagetronExperimentalDedicated-32bit.AppImage
uri_macosserver: armagetronad-experimental-dedicated-0.4.0_alpha_z5488.dmg

uri_tarsrc: armagetronad-experimental-0.4.0_alpha_z5488.tbz
uri_zipsrc: armagetronad-experimental-source-0.4.0_alpha_z5488.zip

---

{% include experimental_build_v1.md %}

### Release Notes

This is from the 0.4 branch of development. Our experimental releases are derived from here.

Changes in AppImage Land: Our AppImage files now are signed and support bandwidth-saving updates via [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). They no longer carry their version in the filename because after an update, that would be a lie.

We now support installation via Flatpak for the 64-bit Linux client. Get the stable versions from [Flathub](https://flathub.org/apps/details/org.armagetronad.ArmagetronAdvanced); [our own repository](https://download.armagetronad.org/docs/flatpak/) has those and also carries the usual test builds.

We started to integrate builds for [macOS](https://wiki.armagetronad.org/index.php?title=MacOS).
Consider them alpha for now, testing and patches very welcome. The current macOS specific issues
can be viewed [on our tracker](https://gitlab.com/armagetronad/armagetronad/-/issues?label_name%5B%5D=macOS).

0.2.9.1.1 worked around a crash bug in the (otherwise splendid) sdl12-compat library.

### Patch Notes


#### Changes since 0.2.9.1.1:

##### Fixed Bugs

 * Ubuntu PPA release candidate builds would overwrite previous stable releases ([#15](https://gitlab.com/armagetronad/armagetronad/-/issues/15))
 * Bad video mode error recovery ([#23](https://gitlab.com/armagetronad/armagetronad/-/issues/23))
 * Crash when entering any game on client on OpenBSD+clang ([#30](https://gitlab.com/armagetronad/armagetronad/-/issues/30))
 * mathexpr.cpp: exp10 not available on OpenBSD ([#31](https://gitlab.com/armagetronad/armagetronad/-/issues/31))
 * Global keyboard actions, like fullscreen toggle, triggered while entering text in text input fields ([#32](https://gitlab.com/armagetronad/armagetronad/-/issues/32))
 * Windows: DPI scaling behavior wrong ([#33](https://gitlab.com/armagetronad/armagetronad/-/issues/33))
 * AppRun script uses ~/.armagetronad as cache directory, should use ${XDG_CACHE_HOME:-~/.cache}/armagetronad ([#43](https://gitlab.com/armagetronad/armagetronad/-/issues/43))
 * Music does not work on SDL_Mixer > 2.0.1 ([#64](https://gitlab.com/armagetronad/armagetronad/-/issues/64))
 * Work around Ubuntu sending spurious window activation messages ([#68](https://gitlab.com/armagetronad/armagetronad/-/issues/68))
 * Desktop/AppData/Icon files are installed in a wrong place during make install ([#73](https://gitlab.com/armagetronad/armagetronad/-/issues/73))
 * Steam: No logging, does not work in pressure vessel ([#77](https://gitlab.com/armagetronad/armagetronad/-/issues/77))
 * "xdg-icon-resource: command not found" on macOS ([#83](https://gitlab.com/armagetronad/armagetronad/-/issues/83))
 * Retina display not working correctly ([#88](https://gitlab.com/armagetronad/armagetronad/-/issues/88))
 * 0.4 macOS build crashes on startup ([#94](https://gitlab.com/armagetronad/armagetronad/-/issues/94))
 * Fullscreen mode: flickering startup ([#95](https://gitlab.com/armagetronad/armagetronad/-/issues/95))
 * Window title sometimes SDL_APP ([#102](https://gitlab.com/armagetronad/armagetronad/-/issues/102))
 * Docker Image: Increase thread stack size ([#104](https://gitlab.com/armagetronad/armagetronad/-/issues/104))
 * Client sends quickly typed chat message with last character missing ([#106](https://gitlab.com/armagetronad/armagetronad/-/issues/106))
 * Fix invulnerability bug handling ([#111](https://gitlab.com/armagetronad/armagetronad/-/issues/111))
 * Fix login prompt not accepting input immediately ([#123](https://gitlab.com/armagetronad/armagetronad/-/issues/123))
 * User data saved in the wrong place on macOS ([#125](https://gitlab.com/armagetronad/armagetronad/-/issues/125))

##### New Features

 * user.cfg fine grained upgrade system ([#61](https://gitlab.com/armagetronad/armagetronad/-/issues/61))
 * Add AppImage zsync update data ([#78](https://gitlab.com/armagetronad/armagetronad/-/issues/78))
 * Provide docker image for the dedicated server ([#79](https://gitlab.com/armagetronad/armagetronad/-/issues/79))
 * Sign the application bundle ([#89](https://gitlab.com/armagetronad/armagetronad/-/issues/89))
 * Add option for low DPI rendering ([#96](https://gitlab.com/armagetronad/armagetronad/-/issues/96))
 * Make window resizable ([#97](https://gitlab.com/armagetronad/armagetronad/-/issues/97))

##### Other Changes

 * Improve sound ([#45](https://gitlab.com/armagetronad/armagetronad/-/issues/45))
 * Pasting does not properly handle unicode input ([#51](https://gitlab.com/armagetronad/armagetronad/-/issues/51))
 * Flatpak adaptions ([#75](https://gitlab.com/armagetronad/armagetronad/-/issues/75))
 * Improve deployment cancellation ([#80](https://gitlab.com/armagetronad/armagetronad/-/issues/80))
 * Crash when votes are issued ([#84](https://gitlab.com/armagetronad/armagetronad/-/issues/84))
 * Better detection for boost-thread ([#85](https://gitlab.com/armagetronad/armagetronad/-/issues/85))
 * Add macOS dmg bundling ([#86](https://gitlab.com/armagetronad/armagetronad/-/issues/86))
 * Integrate macOS build into pipeline ([#87](https://gitlab.com/armagetronad/armagetronad/-/issues/87))
 * Windows debug build crashes on startup ([#91](https://gitlab.com/armagetronad/armagetronad/-/issues/91))
 * 0.2.9 clients lose connection on 0.4 server ([#98](https://gitlab.com/armagetronad/armagetronad/-/issues/98))
 * Backslashes not escaped when saving to user.cfg ([#110](https://gitlab.com/armagetronad/armagetronad/-/issues/110))
 * Internal error in boost::thread::thread(const T &) [T = tMemberFunctionRunnerTemplate<nDNSResolver>] ([#115](https://gitlab.com/armagetronad/armagetronad/-/issues/115))
 * Window size sometimes not set ([#116](https://gitlab.com/armagetronad/armagetronad/-/issues/116))
 * Window drifts when toggling to fullscreen and back ([#117](https://gitlab.com/armagetronad/armagetronad/-/issues/117))
 * macOS DMG Build ([#119](https://gitlab.com/armagetronad/armagetronad/-/issues/119))
 * error: use of undeclared identifier 'finite'; did you mean 'isfinite'? ([#120](https://gitlab.com/armagetronad/armagetronad/-/issues/120))
 * fadeout name improvements ([#121](https://gitlab.com/armagetronad/armagetronad/-/issues/121))
 * Crash on dedicated server startup ([#122](https://gitlab.com/armagetronad/armagetronad/-/issues/122))
 * Disable low latency mode if not waiting for vsync ([#124](https://gitlab.com/armagetronad/armagetronad/-/issues/124))
 * Sound trouble deja entendu ([#130](https://gitlab.com/armagetronad/armagetronad/-/issues/130))

##### Contributors

Armanelgtron, AsciiWolf, Cosimo Cecchi, Dan Church, Daniel Harple, Dave Fancella, Foster McLane, Hugh McMaster, Jip, Luke-Jr, Manuel Moos, Matias Pino, Niklas Karbaum (ai.tron), Viorel-Cătălin Răpițeanu, Vitty, Voodoo, Yann Kaiser, armagetron at ensemble-fnm.de, epsy, guru3, madmax, philippeqc, pnoexz at gmail.com, wrtlprnft, yarrt
