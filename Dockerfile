FROM docker.io/ruby:2.7

# source: https://hub.docker.com/_/ruby/

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /usr/src/jekyll

COPY Gemfile Gemfile.lock ./
RUN bundle install

RUN apt update -y && apt install rsync -y

CMD ["bundle", "exec", "jekyll", "build", "-s", "/source", "-d", "/site"]
