#!/bin/bash

# try running natively
if bundle exec jekyll serve; then exit 0; fi
# fall back to using a container

# prefer podman over docker
DOCKER=docker
podman --help > /dev/null && DOCKER=podman

# use prebuilt package
# from https://github.com/envygeeks/jekyll-docker/blob/master/README.md
export JEKYLL_ROOTLESS=true 
${DOCKER} run --env JEKYLL_ROOTLESS --rm --name armadownload --volume="$PWD:/srv/jekyll" -p 4000:4000 -it docker.io/jekyll/jekyll:4.0.1 jekyll serve --watch --drafts
#${DOCKER} run --env JEKYLL_ROOTLESS --rm --name armadownload --volume="$PWD:/srv/jekyll" --volume="$PWD/${BUNDLE_CACHE}:/usr/local/bundle" -p 4000:4000 -it docker.io/jekyll/jekyll:4.0.1 jekyll serve --watch --drafts

# self-building or using the prebuilt image should also work, but don't for some reason. Oh well.
# build
#${DOCKER} build . -t armadownload
# run
#${DOCKER} run --rm --name armadownload --volume="$PWD:/srv/jekyll" -p 4000:4000 -it armadownload jekyll serve --watch --drafts /srv/jekyll

