---
title: Ubuntu PPA
permalink: /docs/ppa/
description: Ubuntu PPA 
---

PPAs are small, personal repositories for .deb packages. Builds are uploaded to four 
PPAs on Launchpad, installation instructions are behind the links:

 * The [Main PPA](https://launchpad.net/~armagetronad-dev/+archive/ubuntu/ppa) houses 
 releases (package *armagetronad*) as well as alphas, betas and experimentals. After 
 adding the PPA to your system, pick between them via apt. As with the other 
 installation methods, different branches can coexist.
 * The [Staging PPA](https://launchpad.net/~armagetronad-dev/+archive/ubuntu/staging) 
 contains release candidates and builds of actual final releases before they gets added to 
 the main PPA. Add this PPA to your system alongside the main PPA if you want to help 
 test upcoming releases.
 * The [LTS PPA](https://launchpad.net/~armagetronad-dev/+archive/ubuntu/lts) contains builds from the most recent long term support branch. Its structure is parallel to the 
 main PPA. Pick this PPA over the main one if you have a special need to be conservative 
 with your upgrades, for example if you run a server and don't want to check every time 
 what we broke this month.
* The [LTS Staging PPA](https://launchpad.net/~armagetronad-dev/+archive/ubuntu/lts-staging) has the release candidates and staged releases for the LTS PPA.
