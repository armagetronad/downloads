---
title: Current Builds
permalink: /docs/current/
redirect_from: /docs/index.html
description: Current Builds
---

### How To

Check out [How to Install]({% link _docs/install.md %}) to learn how to install the builds linked here;
look into [Branches]({% link _docs/branches.md %}) to learn about the differences between the versions on offer.
If you want to help with testing or development, check out [How to Help]({% link _docs/development.md %}).

### Current Builds

{% include current_CURRENT_stable.md %}
{% include current_CURRENT_rc.md %}
{% include current_CURRENT_beta.md %}
{% include current_CURRENT_alpha.md %}

{%- assign rc_revno = rc_version | split: "_z" | last -%}
{%- assign beta_revno = beta_version | split: "_z" | last -%}
{%- assign current_is_rc = 0 -%}
{%- if rc_revno >= beta_revno -%}{%- assign current_is_rc = 1 -%}{%- endif -%}

 * Current Release: [{{ stable_version }}]({{ stable_link }})
 * {% if current_is_rc > 0 %} Current Release Candidate: [{{ rc_version }}]({{ rc_link }}) {%- else -%} Current Beta Build: [{{ beta_version }}]({{ beta_link }}){% endif %}
 * Current Alpha Build: [{{ alpha_version }}]({{ alpha_link }})

### Current LTS Builds

{% include current_LTS_0283_stable.md %}
{% include current_LTS_0283_rc.md %}
{% include current_LTS_0283_beta.md %}
{% include current_LTS_0283_alpha.md %}

{%- assign rc_revno = rc_version | split: "_z" | last -%}
{%- assign beta_revno = beta_version | split: "_z" | last -%}
{%- assign current_is_rc = 0 -%}
{%- if rc_revno >= beta_revno -%}{%- assign current_is_rc = 1 -%}{%- endif -%}

 * Current LTS Release: [{{ stable_version }}]({{ stable_link }})
 * {% if current_is_rc > 0 %} Current LTS Release Candidate: [{{ rc_version }}]({{ rc_link }}) {%- else -%} Current LTS Beta Build: [{{ beta_version }}]({{ beta_link }}){% endif %}
 * Current LTS Alpha Build: [{{ alpha_version }}]({{ alpha_link }})

### Other Builds

{% include current_EXPERIMENTAL_experimental.md %}

 * Current Experimental Build: [{{ experimental_version }}]({{ experimental_link }})
 