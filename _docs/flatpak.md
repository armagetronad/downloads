---
title: Flatpak
permalink: /docs/flatpak/
description: Flatpak 
---

Flatpak is for 64 bit Linux Clients only. All Flatpak commands accept the optional `--user` parameter, making the operation affect the
current user only and not the whole system.

A word of warning first, though: If you are short on hard drive space or hate bloat,
Flatpak is not for you.
Every Flatpak app requires that you also install a runtime;
we use the most lightweight one with about 700 Mb.
Runtimes are shared between apps so in a world where Flatpak has taken over,
their space usage does not matter that much; you can think of it as a Linux distribution,
or rather a family of distributions, that sits on top of your own Linux system.
The initially large download is minimally compensated for later by savings in downloads for updates;
those use differential compression and will not transfer unchanged data and libraries
again. Still, it is a lot of data over the wire and on your system.

### Flathub
Armagetron Advanced can be installed from [Flathub](https://flathub.org/apps/details/org.armagetronad.ArmagetronAdvanced) using the instruction there, or with

    flatpak install https://download.armagetronad.org/flatpak/armagetronad-flathub.flatpakref

### Own Repository

The most current Armagetron Advanced builds are in our own small repository, you can add it to your system with

    flatpak remote-add --if-not-exists armaflat https://download.armagetronad.org/flatpak/armaflat.flatpakrepo

You can browse the repository in the usual way (`flatpak remote-ls armaflat` on the command line)
and install the builds from the repository:

    # releases
    flatpak install armaflat org.armagetronad.ArmagetronAdvanced//stable

    # release candidates
    flatpak install armaflat org.armagetronad.ArmagetronAdvanced//rc

    # betas
    flatpak install armaflat org.armagetronad.ArmagetronBeta

    # alphas
    flatpak install armaflat org.armagetronad.ArmagetronAlpha

    # experimentals
    flatpak install armaflat org.armagetronad.ArmagetronExperimental

There are more branches for `org.armagetronad.ArmagetronAdvanced`; if you are not interested in having
multiple branches installed without older versions overwriting configuration files of newer versions,
you can install the alpha and beta branch for that. If you want a version that will stick to the 0.2.9 series forever, install stable_0.2.9.

### Direct References

If you don't want to add the repository, you can also install them using these direct references (just click; your system may have an app installed that knows what to do with them):

 * [Stable Releases](/flatpak/armagetronad-stable.flatpakref)
 * [Release Candidates](/flatpak/armagetronad-rc.flatpakref)
 * [Beta Builds](/flatpak/armagetronad-beta.flatpakref)
 * [Alpha Builds](/flatpak/armagetronad-alpha.flatpakref)
 * [Experimental Builds](/flatpak/armagetronad-experimental.flatpakref)

Or over the commandline:

    flatpak install https://download.armagetronad.org/flatpak/armagetronad-stable.flatpakref
    flatpak install https://download.armagetronad.org/flatpak/armagetronad-rc.flatpakref
    flatpak install https://download.armagetronad.org/flatpak/armagetronad-beta.flatpakref
    flatpak install https://download.armagetronad.org/flatpak/armagetronad-alpha.flatpakref
    flatpak install https://download.armagetronad.org/flatpak/armagetronad-experimental.flatpakref

(This will still add the repository, but in a hidden way so it won't appear in GUI browsers.)

### Running

There will be a start menu entry added for you. It is set up so that it can play back debug recordings, you should be
able to assign it to be used when you open an .aarec file.

On the command line, a basic run is done with either one of

    flatpak run org.armagetronad.ArmagetronAdvanced
    flatpak run org.armagetronad.ArmagetronBeta
    flatpak run org.armagetronad.ArmagetronAlpha
    flatpak run org.armagetronad.ArmagetronExperimental

If you have both the release and release candidates installed, you can either select the one you want with
an additional `--branch=rc` or `--branch=stable` argument, or permantently set the one to run with

    flatpak make-current org.armagetronad.ArmagetronAdvanced <branch name>

You have to be aware of the big specialty of Flatpak: Armagetron runs in a sandbox and cannot see
most of your files. That's a problem if you want to record or play back debug recordings. The Flatpak
developers thought of that and implemented file forwarding. Give it the `--file-forwarding` extra
command line argument and everything between @@ and @@ in the command line is made visible. So you can
playback a recording with

    flatpak run --file-forwarding org.armagetronad.ArmagetronAdvanced @@ bug.aarec @@

Creating a recording requires you to create a dummy file, or forwarding will not work:

    touch bug.aarec; flatpak run --file-forwarding org.armagetronad.ArmagetronAdvanced --record @@ bug.aarec @@

And if you want to use custom data directories, things get really hairy. To help you, we have
an external Flatpak that just contains a script for building the correct command line. Install it with

    flatpak install https://download.armagetronad.org/flatpak/armagetronad-launcher.flatpakref

Call it with

    flatpak run org.armagetronad.ArmagetronLauncher <your arguments>

As an additional argument, you can specify `--branch <branch name>`. That selects one
of the non-default branches.

The default configuration data is also not in its usual space, but in the sandboxed areas
`~/.var/app/org.armagetronad.Armagetron*/`. Use the game's "About" page for details.

### Further Remarks

All these packages can coexist; only Release Candidates and Releases share data. `flatpak update` updates you to the current versions.

### Uninstallation

You can get rid of all installed versions with one simple command:

    flatpak uninstall org.armagetronad.

and remove the repository with

    flatpak remote-delete armaflat
