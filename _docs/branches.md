---
title: Branches
permalink: /docs/branches/
description: The different kinds of releases available
---

{% include current_CURRENT_stable.md %}
{% include current_CURRENT_rc.md %}
{% include current_CURRENT_beta.md %}
{% include current_CURRENT_alpha.md %}
{% include current_EXPERIMENTAL_experimental.md %}

Armagetron Advanced is available in different states of development and different qualities. If you want to help with
testing, read [this section]({% link _docs/development.md %}#how-can-i-help-with-testing).

### Releases

These are builds we have a high confidence in being correct and usable. Beginners should stick to them. If a release turns out to have a nasty bug, we do our best to fix it in a timely followup release.

The most recent relase is [{{ stable_version }}]({{ stable_link }}).

### Release Candidates

The stage immediately before a proper release. These are builds we have a high confidence in, but we're leaving them as candidates to give them the last round of testing by volunteers. 

Release candidates and final releases share the same client data.

The most recent relase candidate is [{{ rc_version }}]({{ rc_link }}).

### Betas

The buffer between alphas and release candidates. Beta releases in each development iteration start out identical to alpha releases, but then receive no more features and only bugfixes for a period of time, until they mature into release candidates.

Beta builds use a dedicated place for client data and won't interfere with final releases. You can install betas and final releases in parallel and switch between them at will.

The most recent beta build is [{{ beta_version }}]({{ beta_link }}).

### Alphas

Alpha builds come from a current branch of development. Whenever a developer writes and publishes some code, it results in an alpha build announced here about six hours later. Alphas have all the newest features, but also all of the newest bugs.

Alpha builds use a dedicated place for client data and won't interfere with final releases or betas.

The most recent alpha build is [{{ alpha_version }}]({{ alpha_link }}).

### Experimentals

This category is not very well defined. Currently (2020), experimental builds regularly come from the master branch, but that is expected to change.

The most recent experimental build is [{{ experimental_version }}]({{ experimental_link }}).

### LTS (Long Term Support)

Long term support releases are not intended for individuals to use, but rather for Linux distributors; they are based on old releases and stop receiving new features. However, they still receive security fixes, adaptions to build on newer systems, and important bugfixes.

At this time, we make no concrete promises on how long we supply which kind of update, but the 0.2.8.3 branch was essentially our LTS branch for the past ten years or so and got all of the security and compatibility fixes. Expect something along the line of

- the newest LTS branch gets all the security, compatibility and critical bug fixes
- older LTS branches get all the security fixes for at least two years after the first release frm the next LTS branch
- we will watch which Linux distribution integrates which LTS branch and try to supply at least serverside security fixes for the branches in active use

The only current LTS branch is 0.2.8.3, hopefully soon to be followed by 0.2.9.

{% include current_LTS_0283_stable.md %}
The most recent release from 0.2.8.3 is [{{ stable_version }}]({{ stable_link }}).

### Hacks

Builds from source modified by non-core contributors, or sometimes by core contributors who want to run limited tests.

### Staging

All the build types above are in fact available to try before a proper entry is made for them. You can catch them early in two ways:

 - Fetch them from the [staging area](https://download.armagetronad.org/staging/)
 - Use [Zero Install]({% link _docs/install.md %}#zero-install) and reduce the stability requirement for versions you want to use