---
title: How to Help
permalink: /docs/development/
description: How to help develop and test this game
---

## Who can contribute?

Anyone! You don't have to be an elite player to file a bug report.

On the development side, you need to know that the game is always going to be distributed under the GPL. That
would be too hard to change. If you contribute, your code will also be GPL. In fact, it's GPL the second you make
it available to anyone else. So that's our Contributor License Agreement, "Mind the GPL".

As for the other thing projects are supposed to think about, a Code of Conduct, we don't have one. We do
have [forum rules](https://forums3.armagetronad.net/viewtopic.php?f=45&t=20928) that may pass for one if you squint.

Code or art contributors will get credit in the patch notes and changelog for the first release the contribution
is part of based on the name in the git commits. There is an active [issue](https://gitlab.com/armagetronad/armagetronad/-/issues/65) to extend that to the AUTHORS file.

## How can I help with testing?

The most important thing you can do is to use builds from the [Beta Branch]({% link _docs/branches.md %}#betas)
for your regular play. They should be reasonably safe most of the time, and when they're not, 
that's precicely the time where playtesters need to notice it and file bug reports, because something slipped past the developers there.

Additionally, if you want to give feedback on new features and other changes, 
check out one of the [Alphas]({% link _docs/branches.md %}#alphas) from time to time.
Bigger changes usually get announced on the [forums](https://forums3.armagetronad.net/viewforum.php?f=50).

## How is development organized?

Development currently happens mostly on [GitLab](https://gitlab.com/armagetronad/armagetronad). If you want to mess
with the code on your own, you can either just create a local clone of the repository or fork it. 

Main other interaction points there are the [Issue Tracker](https://gitlab.com/armagetronad/armagetronad/-/issues) and [Merge Requests](https://gitlab.com/armagetronad/armagetronad/-/merge_requests), more on those [below](#workflow).

### Git Branches

These are the git branches used for development:

 * trunk: Development focus. Future 0.4 builds, currently not in a releaseable state.
 * legacy_0.2.9: Source of the current 0.2.9 release series.
 * legacy_0.2.8.3: Source of the 0.2.8.3 LTS release series.

These should be the ones you branch from and request merges to for any new features and most bugs.

The two legacy branches have corresponding beta_ and release_ branches. Trunk is going to get them eventually, too. They are
used to stabilize the source before a release. Check out [this forum post](https://forums3.armagetronad.net/viewtopic.php?f=4&t=38640) for how that works. These branches should be used for regression fixes only, that is fixes of bugs
that were not present in recent previous releases. Fixes for less important bugs as old as time can go to the development 
branches and enter the next release.

### Rules for Issues

We generate our patch notes and CHANGELOG from GitLab issues. From that follows that we want:

 * Every change the user might care about should have an issue associated with it.
 * The issue title should be short and descriptive.
 * Feature request issue titles should not contain fluff words. 
   No "It would be nice if...", "Please..." ", if it's not too much trouble.". 
   Just state the desired change in imperative form: 
   "Tweak death zone spawning" would be a good title.
   Obvious main verbs like "Add", "Make", "Implement" or "Introduce" can be dropped, too:
   "Player Ranking and Matchmaking" would be a fine title.
   If you just see a thing under new features, it's clear it's a thing that was added.
 * Bug titles should state the buggy behavior, "Death zone does not kill" is a good title.
   It should not describe the change the fix needs to be: 
   "Make Death zone kill again" is not such a good title.
 * For further clarification and elaboration, there is the issue description.

 Some people may consider the terseness of the issue titles impolite. Please consider them efficient and to the point instead, and note that the rules also forbid rudeness; "When will you idiots finally fix rubber depletion?" is also not 
 a good title.
 If you feel the need for explicit politeness, the issue description has place for that.
 
 And no worries if your first title does not meet the requirements; we'll edit it accordingly once the issue is fixed. Sometimes, it's not even possible to find a good title until the issue has been through the process.

### Workflow

We mostly stick to this:
 
 * For every change, be it a bug, feature or other change, an issue is created describing it.
 * Discussions that should be settled before actual hacking starts happen on that issue.
 * A developer takes over the issue and starts a new git branch, forked from the desired [target branch](#git-branches), 
   to  work on it.
 * That branch is first pushed only to the developer's own fork of the code.
 * The developer creates a merge request (git push helpfully gives an URI for that).
 * Discussion on the concrete implementation can happen on the merge request; the developer can
   already open the request on half finished work for review/questions by giving it the WIP prefix.
 * One of the small number of project maintainers accepts the merge request.
 * One or several of the commits, last chance being the merge commit, should reference the issue in its commit message.
 * The issue is marked as closed (happens automatically when merged to trunk).

 Depending on the size of the task, all of the steps above are optional. Discussion is often not required. Small tweaks do not need an issue associated with them. Really small changes sometimes are pushed directly to the development branches.

### Rebasing and Squashing

Git offers powerful tools to rewrite branch history before merging it into the development branch. So the question comes up
if and when to use them.

All of the rewrite tools cause issues 
when the source branch has already been merged elsewhere, because under the hood, the rewritten branch is indistinguishable
from a new branch with the same end source tree, but different history and all new hashes. So if one merges the original and
rewritten branches, one doubles each commit. So that's the first rule of rebasing and squashing: If the branch is already
merged elsewhere, don't.

Also, don't rebase/squash if you don't want to for any reason. For example, you want the final history to be just the way 
you implemented it, some warts included. If there are too many warts, we'll bring it up in the merge
request discussion and find a solution.

#### Squashing
Squasing collapses all commits that are new in the merge source into one. GitLab offers a checkbox when you issue a merge
request to have your source branch squashed.

Our policy there is: Respect the merge request submitter's choice.

Rationale: Squashing does not alter history, it removes (irrelevant, one would hope) parts of it. The revision 
that is merged is still one the developer had on the hard drive at some point and had a chance to test in precisely
this form, and the developer is the one knowing whether the intermediate revisions getting squashed away would be of future
value.

#### Rebasing
Noninteractive rebasing picks a new base point for a branch. If you created your work branch two months ago and done 
some work, you can

    git rebase <name of development branch>

to make the git history appear as if you started just now. As far as the graph is concerned, anyway, your commit dates
will stay unaltered by default. The benefit is a neater git history with less parallel branches going next to each other in
`git log --graph`.

Our policy there is: Avoid doing that.

Rationale: Rebasing this way rewrites every one of your commits by adding the changes that happened in the main branch in the
meantime. You now have states in your history you never could test. That's not good. And have you looked at
trunk's `git log --graph`? Does it look like we care about its tidiness?

#### Interactive Rebasing
Interactive rebasing is invoked with `git rebase -i`. It can be done without changing the branching off point with
`git rebase -i --keep-base <name of parent branch>`. This is not the place to go into details, but interactive rebasing
allows you to edit the history of your branch how you like it. For example, you may notice your change from three commits
ago broke the server build; you can commit a fix to that, but the intermediate revisions will stay broken. Now interactive
rebasing allows you to move your fix commit back in time next to the commit that broke things, then squash only those two
together, effectively erasing your error from history completely. It also allows you to change the
commit messages of past commits.

Our policy there is: Sure, why not?

Rationale: The risk of creating broken revisions out of good ones is still there, but minimal and outweighed by the
intended result of fixing broken revisions.

## Other places

We're also present on [GitHub](https://github.com/ArmagetronAd/armagetronad), if that is more your home, the repositories
sync both ways. The issue tracker there is disabled to avoid confusion, but merge requests are accepted from there, too.

Also still in active sync is our [Launchpad Project](https://launchpad.net/armagetronad). We also use it to store downloads
of full releases, but not much else.

[SourceForge](https://sourceforge.net/projects/armagetronad/) nowadays is only used as a build and release archive.