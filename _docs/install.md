---
title: How to Install
permalink: /docs/install/
description: Various ways to download, install and run the game
---

## Cross Platform

### Zero Install

[Zero Install](https://0install.net/) is a system that can automatically download and run 
the latest version of a software package. We support it for Windows, Linux 32 bit and 
Linux 64 bit. Head over to the [main installation feed](https://0install.armagetronad.org/armagetronad.xml) 
for instructions. If you subscribe to that feed, you will get the regular releases by 
default. Also available: [Beta feed](https://0install.armagetronad.org/armagetronad-beta.xml), 
[alpha feed](https://0install.armagetronad.org/armagetronad-alpha.xml) and 
[experimental feed](https://0install.armagetronad.org/armagetronad-experimental.xml).

Zero Install is the most convenient way to get staged builds. For alphas and betas, that 
means you get new builds six hours before anyone else. To get to the staged releases 
from each feed, set the *Preferred stability* to *Developer*.

To get release candidates, either subscribe to 
[their dedicated feed](https://0install.armagetronad.org/armagetronad-rc.xml) 
or subscribe to the main feed and set *Preferred stability* to *Testing*.

### Steam

Armagetron Advanced is available on Steam as [Retrocycles](https://store.steampowered.com/app/1306180/Retrocycles/). 
You can switch between branches the *Betas* tabs in *Properties*. *NONE* gives you regular releases, *staging* gives
you staged releases and release candidates. *Alpha*, *beta* and *experimental* are what they are here.

Steam sadly has no support for install several branches in parallel, on one Steam installation anyway.

### itch.io

On itch.io, it's simply [Armagetron Advanced](https://armagetronad.itch.io/armagetronad). You can use the itch client
to receive automatic updates. The branch name is in the filename and matches the designation here.

### Which of these do I pick?

If you already have a Steam or itch client running all of the time, go with that. Both system have incremental updates,
meaning that when an update comes out, you only download the bits that are new. That's neat. 

If you don't have any of those, give Zero Install a try. Of the three, it has the lowest system overhead and makes up
for the lack of incremental downloads with raw speed and flexibility. If that doesn't make you happy, move on to itch.io.
Steam is slowest to start up and regularly does self-updates with download sizes in the hundreds of megabytes, so I would
not recommend it for this one game.

Ubuntu users can additionally get a version that's best for their system from the PPA. 

## Windows Installer

We supply installer executables for each build. Download and execute them. Versions from 
different branches generally do not overwrite each other, so you can have alpha and 
release builds in parallel.

## Linux

### Ubuntu PPA
Is described on [its own page](/docs/ppa/).


### AppImage

For direct download from the individual build pages, we offer [AppImages](https://appimage.org/). They are self contained executables that run on their own and contain all of the data, too. Just download one, flag it as executable and run.
Your file manager will have a checkbox to mark the download as executable; failing that,
you can use the command line and

    chmod +x <name of download> 

More information can be found on the [AppImageHub](https://appimage.github.io/Armagetron_Advanced/#installation) page.

NixOS note: Use the `appimage-run` wrapper to run AppImages.

### Flatpak
Is described on [its own page](/docs/flatpak/).

### Containers for servers
A complex topic, also [on its own page](/docs/container/). So far, for alpha builds only.

### Source

If any of the above methods don't work for your or you just like to compile your own programs, there is the source tarball. To use that, download it and in some empty directory, execute

    tar -xjf <path>/<archive name>.tbz
    mkdir build
    cd build
    ../<archive name>/configure
    make -j `nproc`

You can run the game directly from the build directory with

    make run

Or install it on the system with

    sudo make install



