#!/bin/bash

# brings the Gemfile.lock up to date
# source: https://hub.docker.com/_/ruby/

docker run --rm -v "$PWD":/usr/src/app -w /usr/src/app ruby:2.7 bundle install
