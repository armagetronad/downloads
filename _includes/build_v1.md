
Built from [{{page.git_reference}}]({{page.git_revurl_base}}{{page.git_rev}})

### Game Client Downloads

{% if page.uri_winclient %}
**Windows**: [{{page.uri_winclient}}]({{page.uri_base}}{{page.uri_winclient}})
{% endif %}

{% if page.uri_lin64client %}
**Linux 64 Bit**: [{{page.uri_lin64client}}]({{page.uri_base}}{{page.uri_lin64client}})
{% endif %}

{% if page.uri_lin32client %}
**Linux 32 Bit**: [{{page.uri_lin32client}}]({{page.uri_base}}{{page.uri_lin32client}})
{% endif %}

{% if page.uri_macosclient %}
**macOS**: [{{page.uri_macosclient}}]({{page.uri_base}}{{page.uri_macosclient}})
{% endif %}

### Game Server Downloads

{% if page.uri_winserver %}
**Windows**: [{{page.uri_winserver}}]({{page.uri_base}}{{page.uri_winserver}})
{% endif %}

{% if page.uri_lin64server %}
**Linux 64 Bit**: [{{page.uri_lin64server}}]({{page.uri_base}}{{page.uri_lin64server}})
{% endif %}

{% if page.uri_lin32server %}
**Linux 32 Bit**: [{{page.uri_lin32server}}]({{page.uri_base}}{{page.uri_lin32server}})
{% endif %}

{% if page.uri_macosserver %}
**macOS**: [{{page.uri_macosserver}}]({{page.uri_base}}{{page.uri_macosserver}})
{% endif %}

### Source Downloads

{% if page.uri_tarsrc %}
**Tarball**: [{{page.uri_tarsrc}}]({{page.uri_base}}{{page.uri_tarsrc}})
{% endif %}

{% if page.uri_zipsrc %}
**ZIP for Windows Builds**: [{{page.uri_zipsrc}}]({{page.uri_base}}{{page.uri_zipsrc}})
{% endif %}


