<div class="well">
    <h4>Recent News</h4>
    <ul class="list-unstyled post-list-container">
        {%- comment -%}Keep tabs on the number of reserved slots per news category{%- endcomment -%}
        {%- assign lts_reserve = 1 -%}
        {%- assign release_reserve = 2 -%}
        {%- assign rc_reserve_hard = 2 -%}
        {%- assign rc_reserve = 1 -%}
        {%- assign beta_reserve_hard = 3 -%}
        {%- assign beta_reserve = 1 -%}
        {%- assign alpha_reserve_hard = 3 -%}
        {%- assign alpha_reserve = 1 -%}
        {%- assign blog_reserve = 3 -%}
        {%- assign total_left = lts_reserve |plus:release_reserve |plus:rc_reserve |plus:beta_reserve |plus:alpha_reserve |plus:blog_reserve |plus:5 -%}

        {%- for post in site.posts -%}
            {%- assign show = true -%}
            {%- comment -%}Decrement the reserved counter belonging to the category of the post, thus freeing a slot that gets immediately used{%- endcomment -%}
            {%- if post.categories contains 'blog' -%}
                {%- if blog_reserve > 0 -%}{%- assign blog_reserve = blog_reserve | minus:1 -%}{%- endif -%}
            {%- elsif post.categories contains 'release' -%}
                {%- if release_reserve > 0 -%}{%- assign release_reserve = release_reserve | minus:1 -%}{%- endif -%}
            {%- elsif post.categories contains 'lts' -%}
                {%- if lts_reserve > 0 -%}{%- assign lts_reserve = lts_reserve | minus:1 -%}{%- endif -%}
            {%- elsif post.categories contains 'rc' -%}
                {%- if rc_reserve_hard > 0 -%}{%- assign rc_reserve_hard = rc_reserve_hard | minus:1 -%}{%- else -%}{%- assign show = false -%}{%- endif -%}
                {%- if rc_reserve > 0 -%}{%- assign rc_reserve = rc_reserve | minus:1 -%}{%- endif -%}
            {%- elsif post.categories contains 'beta' -%}
                {%- if beta_reserve_hard > 0 -%}{%- assign beta_reservev = beta_reserve_hard | minus:1 -%}{%- else -%}{%- assign show = false -%}{%- endif -%}
                {%- if beta_reserve > 0 -%}{%- assign beta_reserve = beta_reserve | minus:1 -%}{%- endif -%}
            {%- elsif post.categories contains 'build' -%}
                {%- if alpha_reserve_hard > 0 -%}{%- assign alpha_reserve_hard = alpha_reserve_hard | minus:1 -%}{%- else -%}{%- assign show = false -%}{%- endif -%}
                {%- if alpha_reserve > 0 -%}{%- assign alpha_reserve = alpha_reserve | minus:1 -%}{%- endif -%}
            {%- endif -%}
            {%- assign left = total_left |minus:blog_reserve |minus:lts_reserve |minus:release_reserve |minus:rc_reserve |minus:beta_reserve |minus:alpha_reserve -%}
            {%- if show and left > 0 -%}
                {% assign total_left = total_left | minus:1 %}
                <li><a href="{{ post.url | relative_url }}" {% if page.title==post.title %} class="active" {% endif %}>{{ post.title }}</a></li>
            {%- endif -%}
        {%- endfor -%}
        <li>&nbsp;</li>
        <li><a href="{{ "/allnews" | relative_url }}">All news ...</a></li>
        <li><a href="{{ "/allreleases" | relative_url }}">All releases ...</a></li>
        <li><a href="{{ "/allblog" | relative_url }}">All blog entries ...</a></li>
        <li><a href="{{ "/allbuilds" | relative_url }}">All builds (release, beta, alpha...) ...</a></li>
        <li><a href="{{ "/alllts" | relative_url }}">All LTS releases ...</a></li>
    </ul>
</div>
