<div class="well">
    <h4>Recent Posts</h4>
    <ul class="list-unstyled post-list-container">
        {% for post in site.posts limit:10 %}
        {% if post.categories contains 'news' %}
        <li><a href="{{ post.url | relative_url }}" {% if page.title==post.title %} class="active" {% endif %}>{{ post.title }}</a></li>
        {% endif %}
        {% endfor %}
        <li><a href="{{ "/allnews" | relative_url }}">All Posts ...</a></li>
    </ul>
</div>
