{% include build_v1.md %}

This is a [Release Candidate]({% link _docs/branches.md %}#release-candidates) for ironing out the last breaking changes before an actual release.