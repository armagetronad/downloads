{% include build_v1.md %}

This is an [LTS]({% link _docs/branches.md %}#lts-long-term-support) build intended for Linux distributors and very conservative players.
